.PHONY: clean system-packages python-packages install tests run all

clean:  DEL /S /Q *.pyc *.log

system-packages:    sudo apt install python-pip -y

python-packages:    pip install -r requirements.txt

install:    system-packages python-packages

tests:  python manage.py test

run:    python manage.py run

all:    clean install tests run