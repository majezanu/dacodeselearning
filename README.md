# dacodeselearning
This project is a backend application developed in python whose purpose is to 
provide web services to admin an e-learning platform. CRUD web services were
implemented for courses, lessons, questions, professors and students. As a
business rule it was established that only professors can admin these CRUD,
so an authentication system based on JWT was also implemented.

It was decided to implement the API with the Flask framework, due o its lightness
and ease. In addition, other libraries such as Flask-Restplus were used,
which facilities the creation of an API, as well as helping to document it 
using Swagger. The SQLAlchemy library was also used, wich allows the connection
and creation of databases and their management through their ORM.

Some unit tests were implemented to corroborate that the project configuration
is correct and to test the functioning of some functions.

A configuration file was implemented to handle some important enviroment variables,
this to be able to have three main environments, namely development, testing and
production.

A file was also implemented to better manage the environments mentioned above.
This file has the name "manage.py" and its located in the root of the project,
with this file you can execute the following commands for the installation and
execution of the project:

For installation of dependencies, you can run this commando:
    sudo apt install python-pip -y
    pip install -r requirements.txt
            or
    pipenv install

Then for run the project, you can use this commands:
    python manage.py test (For testing)
    python manage.py run (For development)

Then you can visit the localhost:8888/api and see the Swagger page for manual
test the API

