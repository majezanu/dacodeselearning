from flask_restplus import fields
from app.api.restplus import api
from app.api.serializers import pagination
from app.api.lessons.serializers import lesson

question_min = api.model('Question min', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a lesson'),
    'name': fields.String(required=True, description='Lesson name'),

})

question = api.inherit('Question', question_min, {
    'lesson': fields.Nested(lesson)
})


question_create = api.model('Lesson create', {
    'name': fields.String(required=True, description='Question name'),
    'lesson_id': fields.Integer(description='The unique identifier of the lesson'),
    'score': fields.Integer(required=False, description='Score question')
})

page_of_lesson = api.inherit('Page of lesson', pagination, {
    'items': fields.List(fields.Nested(question))
})