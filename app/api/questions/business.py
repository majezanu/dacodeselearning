from app.database.repository.LessonRepository import LessonRepository
from app.database.models.Lesson import Lesson
from app.database.repository.LessonSuscriptionRepository import LessonSuscriptionRepository
from app.database.repository.QuestionsRepository import QuestionRepository
from app.database.models.Question import Question

def get_all():
    return QuestionRepository.all()


def update(id, data):
    question = QuestionRepository.find_one(id)
    if question is None:
        response_object = {
            'status': 'fail',
            'message': 'Question with this id not found.',
        }
        return response_object, 404
    else:
        question.name = data.get('name')
        question.lesson_id = data.get('lesson_id')
        question.score = data.get('score')
        return QuestionRepository.save(question)


def delete(id):
    question = QuestionRepository.find_one(id)
    if question is None:
        response_object = {
            'status': 'fail',
            'message': 'Question with this id not found.',
        }
        return response_object, 404
    else:
        QuestionRepository.delete(question)
        response_object = {
            'status': 'success',
            'message': 'Succesfully deleted question',
        }
        return response_object, 204


def create(data):
    question = QuestionRepository.find_one_by_name(data.get('name'))
    if not question:
        question = Question(
            name = data.get('name'),
            lesson_id = data.get('lesson_id'),
            score  = data.get('score')
        )
        QuestionRepository.save(question)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Already exist a question with this name',
        }
        return response_object, 401


def get_one_by_id(id):
    question = QuestionRepository.find_one(id)
    if question is None:
        response_object = {
            'status': 'fail',
            'message': 'Question with this id not found.',
        }
        return response_object, 404
    else:
        return question, 200


# def check_if_can_access(lesson_id, student_id):
#     lesson = LessonRepository.find_one(lesson_id)
#     if lesson.previous_lesson is None or LessonSuscriptionRepository.is_previous_passed(student_id, lesson.previous_lesson_id):
#         return True
#     else:
#         return False