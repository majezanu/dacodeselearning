from app.database.repository.StudentRepository import StudentRepository
from app.database.repository.CourseRepository import CourseRepository
from app.database.models.CourseSuscription import  CourseSuscription
from app.database.repository.CourseSuscriptionRepository import  CourseSuscriptionRepository
from app.database.repository.LessonSuscriptionRepository import LessonSuscriptionRepository
from app.api.lessons.business import check_if_can_access as check_if_can_access_lesson
from app.database.models.LessonSuscription import LessonSuscription
from app.api.courses.business import check_if_can_access
from app.database.models.Student import Student
from app.database.repository.LessonRepository import  LessonRepository


def get_all():
    return StudentRepository.all()


def update(id, data):
    student = StudentRepository.find_one(id)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        student.name = data.get('name')
        student.email = data.get('email')
        return StudentRepository.save(student)


def delete(id):
    student = StudentRepository.find_one(id)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        StudentRepository.delete(student)
        response_object = {
            'status': 'success',
            'message': 'Succesfully deleted student',
        }
        return response_object, 204


def create(data):
    student = StudentRepository.find_one_by_name(data.get('name'))
    if not student:
        student = Student(
            name = data.get('name'),
            email = data.get('email'),
        )
        StudentRepository.save(student)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Already exist a student with this name',
        }
        return response_object, 401


def get_one_by_id(id):
    student = StudentRepository.find_one(id)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        return student, 200


def course_suscription(data, id):
    student = StudentRepository.find_one(id)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        courses = data.get('courses')
        for course in courses:
            suscription = CourseSuscriptionRepository.find_one_by_student_and_course(id, course)
            if suscription is not None:
                continue
            suscription = CourseSuscription(
                course, id, False, check_if_can_access(course, student.id)
            )
            CourseSuscriptionRepository.save(suscription)


def get_course_suscription(id):
    student = StudentRepository.find_one(id)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        courses = []
        for course in student.courses:
            suscription = CourseSuscriptionRepository.find_one_by_student_and_course(id, course.id)
            suscription.can_access = check_if_can_access(course.id, student.id)
            CourseSuscriptionRepository.save(suscription)
            ob = {
                'id': course.id,
                'name': course.name,
                'can_access':suscription.can_access,
                'approved':suscription.approved
            }
            courses.append(ob)
        return courses, 200


def lesson_suscription(data, id):
    student = StudentRepository.find_one(id)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        lessons = data.get('lessons')
        for lesson in lessons:
            suscription = LessonSuscriptionRepository.find_one_by_student_and_lesson(id, lesson)
            if suscription is not None:
                continue
            suscription = LessonSuscription(
                lesson, id, False, check_if_can_access_lesson(lesson, student.id)
            )
            LessonSuscriptionRepository.save(suscription)


def get_lesson_questions(student_id, lesson_id):
    student = StudentRepository.find_one(student_id)

    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    if check_if_can_access_lesson(lesson_id, student_id):
        lesson = LessonRepository.find_one(lesson_id)
        return lesson.questions
    else:
        response_object = {
            'status': 'fail',
            'message': 'Student cant acces to this resource',
        }
        return response_object, 401


def get_lesson_suscription(id, courseId):
    student = StudentRepository.find_one(id)
    course = CourseRepository.find_one(courseId)
    if student is None:
        response_object = {
            'status': 'fail',
            'message': 'Student with this id not found.',
        }
        return response_object, 404
    else:
        lessons = []
        for lesson in course.lessons:
            suscription = LessonSuscriptionRepository.find_one_by_student_and_lesson(id, lesson.id)
            suscription.can_access = check_if_can_access_lesson(lesson.id, student.id)
            LessonSuscriptionRepository.save(suscription)
            ob = {
                'id': lesson.id,
                'name': lesson.name,
                'can_access':suscription.can_access,
                'approved':suscription.approved
            }
            lessons.append(ob)
        return lessons, 200

