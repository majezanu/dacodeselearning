import logging
from flask import request
from flask_restplus import Resource
from app.api.restplus import api
from serializers import student, student_create, course_suscription_model, \
    page_of_student, course_suscription_result, lesson_suscription_model
from app.api.questions.serializers import question_min
from business import get_all, create, get_one_by_id, update, delete, course_suscription, get_course_suscription, \
    lesson_suscription, get_lesson_suscription, get_lesson_questions
from app.util.decorator import token_required

log = logging.getLogger(__name__)

ns = api.namespace('student', description='Operations related to e-learning students')


@ns.route('/')
class StudentList(Resource):
    @api.marshal_list_with(student)
    @api.doc(security=None)
    def get(self):
        """ Returns a list of students """
        return get_all()

    @api.response(201, 'Student successfully created.')
    @api.expect(student_create, validate=True)
    @token_required
    def post(self):
        """ Creates a new student """
        data = request.json
        return create(data)


@ns.route('/<id>')
@api.param('id', 'The Student identifier')
class StudentItem(Resource):

    @api.marshal_with(student)
    @api.doc(security=None)
    def get(self, id):
        """ Get a student given its identifier """
        return get_one_by_id(id)

    @api.marshal_with(student)
    @api.expect(student_create, validate=True)
    @token_required
    def put(self, id):
        """ Update a student given its identifier """
        data = request.json
        return update(id, data)

    @token_required
    def delete(self, id):
        """ Delete a student given its identifier """
        return delete(id)


@ns.route('/<id>/course-suscription')
@api.param('id', 'The Student identifier')
class StudentCourseSuscription(Resource):

    @api.expect(course_suscription_model, validate=True)
    @api.response(201, 'Student-course suscription successfully created.')
    @token_required
    def post(self, id):
        """ Creates a new student-courses suscription """
        data = request.json
        course_suscription(data, id)

    @api.response(200, 'Student-courses suscriptions')
    @api.marshal_list_with(course_suscription_result)
    def get(self, id):
        """ GEt all student-courses suscription """
        return get_course_suscription(id)


@ns.route('/<id>/lesson-suscription/<courseId>')
@api.param('id', 'The Student identifier')
@api.param('courseId', 'The Course identifier')
class StudentLessonSuscription(Resource):

    @api.expect(lesson_suscription_model, validate=True)
    @api.response(201, 'Student-course suscription successfully created.')
    @token_required
    def post(self, id):
        """ Creates a new student-courses suscription """
        data = request.json
        lesson_suscription(data, id)

    @api.response(200, 'Student-lesson suscriptions')
    @api.marshal_list_with(course_suscription_result)
    def get(self, id,courseId ):
        """ Get all student-lessons suscription """
        return get_lesson_suscription(id, courseId)


@ns.route('/<id>/lesson-details/<lessonId>')
@api.param('id', 'The Student identifier')
@api.param('lessonId', 'The Course identifier')
class StudentLessonDetails(Resource):

    @api.response(200, 'Student-lesson details')
    @api.marshal_list_with(question_min)
    def get(self, id, lessonId):
        """ Get all student-lessons suscription """
        return get_lesson_questions(id, lessonId)
