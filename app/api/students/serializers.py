from flask_restplus import fields
from app.api.restplus import api
from app.api.serializers import pagination
from app.api.courses.serializers import course

student_create = api.model('Student create', {
    'name': fields.String(required=True, description='Student name'),
    'email': fields.String(required=True, description='Student email'),
})

student = api.inherit('Student',student_create, {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a student'),
})

page_of_student = api.inherit('Page of students', pagination, {
    'items': fields.List(fields.Nested(student))
})

course_suscription_model = api.model('List of courses', {
    'courses': fields.List(fields.Integer)
})

course_suscription_result = api.inherit('Courses can acces',course, {
    'can_access': fields.Boolean(readonly=True, description='The courses that student can access'),
    'approved': fields.Boolean(readonly=True, description='The courses that student has approved')

})

lesson_suscription_model = api.model('List of courses', {
    'lessons': fields.List(fields.Integer)
})

