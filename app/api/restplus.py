import logging
import traceback
from flask_restplus import Api
from app import config
from sqlalchemy.orm.exc import NoResultFound

log = logging.getLogger(__name__)
authorizations = {
    'Bearer Auth': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': 'Professor authorization'
    },
}
api = Api(version='0.0.2',
          title='DaCodes E-Learning API',
          description='An API for e-learning professors to manage courses '
                      'configuration and performance reviews and, for students,'
                      'to take courses when using frontend ',
          security='Bearer Auth',
          authorizations=authorizations)


@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)

    if not config.Config.DEBUG:
        return {'message': message}, 500


@api.errorhandler(NoResultFound)
def database_not_found_error_handler(e):
    log.warning(traceback.format_exc())
    return {'message': 'A database result was required but none was found.'}, 404