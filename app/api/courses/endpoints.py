import logging
from flask import request
from flask_restplus import Resource
from app.api.restplus import api
from serializers import course_min, course_create
from business import get_all, create, get_one_by_id, update, delete
from app.util.decorator import token_required

log = logging.getLogger(__name__)

ns = api.namespace('courses', description='Operations related to e-learning courses')

parser = ns.parser()
parser.add_argument('Authorization', type=str, location='headers', help='Bearer Access Token', required=True)


@ns.route('/')
class CoursesList(Resource):
    @api.marshal_list_with(course_min)
    @api.doc(security=None)
    def get(self):
        """ Returns a list of courses """
        return get_all()

    @api.response(201, 'Course successfully created.')
    @api.expect(course_create, validate=True)
    @token_required
    def post(self):
        """ Creates a new course """
        data = request.json
        return create(data)


@ns.route('/<id>')
@api.param('id', 'The Course identifier')
class CourseItem(Resource):

    @api.marshal_with(course_min)
    @api.doc(security=None)
    def get(self, id):
        """ Get a course given its identifier """
        return get_one_by_id(id)

    @api.marshal_with(course_min)
    @api.expect(course_create, validate=True)
    @token_required
    def put(self, id):
        """ Update a course given its identifier """
        data = request.json
        return update(id, data)

    @token_required
    def delete(self, id):
        """ Delete a course given its identifier """
        return delete(id)