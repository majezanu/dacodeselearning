from flask_restplus import fields
from app.api.restplus import api
from app.api.serializers import pagination

course = api.model('Course', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a course'),
    'name': fields.String(required=True, description='Course name')
})
course_min = api.inherit('Course min', course, {
    'previous_course': fields.Nested(api.model('Previous course', {
        'id': fields.Integer(readOnly=True, description='The unique identifier of a course'),
        'name': fields.String(required=True, description='Course name')
    }))
})

course_create = api.model('Course create', {
    'name': fields.String(required=True, description='Course name'),
    'previous_course_id': fields.Integer(description='The unique identifier of a previous course'),
})

page_of_courses = api.inherit('Page of courses', pagination, {
    'items': fields.List(fields.Nested(course_min))
})