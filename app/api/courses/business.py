from app.database.repository.CourseRepository import CourseRepository
from app.database.repository.CourseSuscriptionRepository import CourseSuscriptionRepository
from app.database.models.Course import Course


def get_all():
    return CourseRepository.all()


def update(id, data):
    course = CourseRepository.find_one(id)
    if course is None:
        response_object = {
            'status': 'fail',
            'message': 'Course with this id not found.',
        }
        return response_object, 404
    else:
        course.name = data.get('name')
        course.previous_course_id = data.get('previous_course_id')
        return CourseRepository.save(course)


def delete(id):
    course = CourseRepository.find_one(id)
    if course is None:
        response_object = {
            'status': 'fail',
            'message': 'Course with this id not found.',
        }
        return response_object, 404
    else:
        CourseRepository.delete(course)
        response_object = {
            'status': 'success',
            'message': 'Succesfully deleted course',
        }
        return response_object, 204


def create(data):
    course = CourseRepository.find_one_by_name(data.get('name'))
    if not course:
        course = Course(
            name=data.get('name'),
            previous_course_id=data.get('previous_course_id')
        )
        CourseRepository.save(course)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Already exist a course with this name',
        }
        return response_object, 401


def get_one_by_id(id):
    course = CourseRepository.find_one(id)
    if course is None:
        response_object = {
            'status': 'fail',
            'message': 'Course with this id not found.',
        }
        return response_object, 404
    else:
        return course, 200


def check_if_can_access(course_id, student_id):
    course = CourseRepository.find_one(course_id)
    if course.previous_course is None or CourseSuscriptionRepository.is_previous_passed(student_id, course.previous_course.id):
        return True
    else:
        return False

