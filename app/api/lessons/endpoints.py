import logging
from flask import request
from flask_restplus import Resource
from app.api.restplus import api
from serializers import lesson, lesson_min, lesson_create
from business import get_all, create, get_one_by_id, update, delete
from app.util.decorator import token_required

log = logging.getLogger(__name__)

ns = api.namespace('lessons', description='Operations related to e-learning lessons')

parser = ns.parser()
parser.add_argument('Authorization', type=str, location='headers', help='Bearer Access Token', required=True)


@ns.route('/')
class LessonList(Resource):
    @api.marshal_list_with(lesson_min)
    @api.doc(security=None)
    def get(self):
        """ Returns a list of lessons """
        return get_all()

    @api.response(201, 'Lesson successfully created.')
    @api.expect(lesson_create, validate=True)
    @token_required
    def post(self):
        """ Creates a new lesson """
        data = request.json
        return create(data)


@ns.route('/<id>')
@api.param('id', 'The Lesson identifier')
class LessonItem(Resource):

    @api.marshal_with(lesson_min)
    @api.doc(security=None)
    def get(self, id):
        """ Get a lesson given its identifier """
        return get_one_by_id(id)

    @api.marshal_with(lesson_min)
    @api.expect(lesson_create, validate=True)
    @token_required
    def put(self, id):
        """ Update a lesson given its identifier """
        data = request.json
        return update(id, data)

    @token_required
    def delete(self, id):
        """ Delete a lesson given its identifier """
        return delete(id)