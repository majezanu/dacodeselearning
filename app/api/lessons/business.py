from app.database.repository.LessonRepository import LessonRepository
from app.database.models.Lesson import Lesson
from app.database.repository.LessonSuscriptionRepository import LessonSuscriptionRepository


def get_all():
    return LessonRepository.all()


def update(id, data):
    lesson = LessonRepository.find_one(id)
    if lesson is None:
        response_object = {
            'status': 'fail',
            'message': 'Lesson with this id not found.',
        }
        return response_object, 404
    else:
        lesson.name = data.get('name')
        lesson.course_id = data.get('course_id')
        lesson.previous_lesson_id = data.get('previous_lesson_id')
        return LessonRepository.save(lesson)


def delete(id):
    lesson = LessonRepository.find_one(id)
    if lesson is None:
        response_object = {
            'status': 'fail',
            'message': 'Lesson with this id not found.',
        }
        return response_object, 404
    else:
        LessonRepository.delete(lesson)
        response_object = {
            'status': 'success',
            'message': 'Succesfully deleted lesson',
        }
        return response_object, 204


def create(data):
    lesson = LessonRepository.find_one_by_name(data.get('name'))
    if not lesson:
        lesson = Lesson(
            name = data.get('name'),
            course_id = data.get('course_id'),
            previous_lesson_id = data.get('previous_lesson_id')
        )
        LessonRepository.save(lesson)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Already exist a lesson with this name',
        }
        return response_object, 401


def get_one_by_id(id):
    lesson = LessonRepository.find_one(id)
    if lesson is None:
        response_object = {
            'status': 'fail',
            'message': 'Lesson with this id not found.',
        }
        return response_object, 404
    else:
        return lesson, 200


def check_if_can_access(lesson_id, student_id):
    lesson = LessonRepository.find_one(lesson_id)
    if lesson.previous_lesson is None or LessonSuscriptionRepository.is_previous_passed(student_id, lesson.previous_lesson_id):
        return True
    else:
        return False