from flask_restplus import fields
from app.api.restplus import api
from app.api.serializers import pagination
from app.api.courses.serializers import course

lesson = api.model('Lesson', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a lesson'),
    'name': fields.String(required=True, description='Lesson name'),
    'course': fields.Nested(course)
})

lesson_min = api.inherit('Lesson min', lesson, {
    'previous_lesson': fields.Nested(api.model('Previous lesson', {
        'id': fields.Integer(readOnly=True, description='The unique identifier of a lesson'),
        'name': fields.String(required=True, description='Course name')
    }))
})

lesson_create = api.model('Lesson create', {
    'name': fields.String(required=True, description='Lesson name'),
    'previous_lesson_id': fields.Integer(description='The unique identifier of a previous lesson'),
    'course_id': fields.Integer(description='The unique identifier of the lesson')
})

page_of_lesson = api.inherit('Page of lesson', pagination, {
    'items': fields.List(fields.Nested(lesson_min))
})