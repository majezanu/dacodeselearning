from flask_restplus import fields
from app.api.restplus import api
from app.api.serializers import pagination

professor_min = api.model('Professor', {
    'name': fields.String(required=True, description='Professor name'),
    'email': fields.String(required=True, description='Professor email'),
})
professor_create = api.inherit('Professor create', professor_min,{
    'password': fields.String(required=True, description='Professor password'),
})

professor_result = api.inherit('Professor result', professor_min,{
    'id': fields.Integer(readOnly=True, description='The unique identifier of a professor')
})


page_of_professors = api.inherit('Page of professors', pagination, {
    'items': fields.List(fields.Nested(professor_result))
})

professor_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
})