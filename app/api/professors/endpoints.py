import logging
from flask import request
from flask_restplus import Resource
from app.api.restplus import api
from business import create, get_all, get_one_by_id, login, logout
from serializers import professor_create, professor_result, professor_auth
from app.util.decorator import token_required

log = logging.getLogger(__name__)

ns = api.namespace('professors', description ='Operations related to e-learning professors')

parser = ns.parser()
parser.add_argument('Authorization', type=str, location='headers', help='Bearer Access Token', required=True)
@ns.route('/')
class ProfessorsList(Resource):
    @api.marshal_list_with(professor_result)
    @api.doc(security=None)
    def get(self):
        """ Returns a list of professors """
        return get_all()

    @api.response(201, 'Professor successfully created.')
    @api.expect(professor_create, validate=True)
    @api.doc(security=None)
    def post(self):
        """ Creates a new professor """
        data = request.json
        return create(data)


@ns.route('/<id>')
@api.param('id', 'The professor identifier')
@api.response(404, 'Professor not found')
class ProfessorItem(Resource):

    @api.marshal_with(professor_result)
    @token_required
    def get(self, id):
        """ Get a professor given its identifier """
        return get_one_by_id(id)


@ns.route('/login')
class ProfessorLogin(Resource):
    @api.expect(professor_auth, validate=True)
    @api.doc(security=None)
    def post(self):
        """ Professor Login Resource """
        data = request.json
        return login(data)


@ns.route('/logout')
class ProfessorLogout(Resource):
    @token_required
    def post(self):
        """ Professor Logout Resource """
        auth_header = request.headers.get('Authorization')
        return logout(auth_header)

