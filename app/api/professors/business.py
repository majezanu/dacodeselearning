from app.database.repository.ProfessorRepository import ProfessorRepository, Professor
from app.database.repository.BlackListTokenRepository import BlackListTokenRepository


def create(data):
    professor = ProfessorRepository.find_one_by_name(data.get('name'))
    if not professor:
        professor = Professor(
            name=data.get('name'),
            email=data.get('email'),
            password=data.get('password')
        )
        return ProfessorRepository.save(professor)
    else:
        response_object = {
            'status': 'fail',
            'message': 'Professor already exists. Please Log in.',
        }
        return response_object, 401


def get_all():
    return ProfessorRepository.all()


def get_one_by_token(request):
    data, status = get_logged_in_professor(request)
    return data.get('data')


def get_one_by_id(id):
    professor = ProfessorRepository.find_one(id)
    if professor is None:
        response_object = {
            'status': 'fail',
            'message': 'Professor with this id not found.',
        }
        return response_object, 404
    else:
        return professor, 200


def login(data):
    try:
        professor = ProfessorRepository.find_one_by_email(data.get('email'))
        if professor and professor.check_password(data.get('password')):
            auth_token = professor.encode_auth_token(professor.id)
            if auth_token:
                response_object = {
                    'status': 'success',
                    'message': 'Successfully logged in.',
                    'Authorization': auth_token.decode()
                }
            return response_object, 200
        if not professor:
            response_object = {
                'status': 'fail',
                'message': 'Professor not found.'
            }
            return response_object, 404
        else:
            response_object = {
                'status': 'fail',
                'message': 'email or password does not match.'
            }
            return response_object, 401
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Try again'
        }
        return response_object, 500


def logout(data):
    if data:
        auth_token = data.split(" ")[1]
    else:
        auth_token = ''
    if auth_token:
        resp = Professor.decode_auth_token(auth_token)
        if not isinstance(resp, str):
            return BlackListTokenRepository.save_token(token=auth_token)
        else:
            response_object = {
                'status': 'fail',
                'message': resp
            }
            return response_object, 401
    else:
        response_object = {
            'status': 'fail',
            'message': 'Provide a valid auth token. '
        }
        return response_object, 403


def get_logged_in_professor(request):
    request_auth_token = request.headers.get('Authorization')
    if (request_auth_token is not None) and (" " in request_auth_token):
        auth_token = request_auth_token.split(" ")[1]
        if auth_token:
            resp = Professor.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                professor = Professor.query.filter_by(id=resp).first()
                response_object = {
                    'status': 'success',
                    'data': {
                        professor
                    }
                }
                return response_object, 200
            response_object = {
                'status': 'failed',
                'message': resp
            }
            return response_object, 401
        else:
            response_object = {
                'status': 'fail',
                'message': 'Provide a valid auth token, professor'
            }
            return response_object, 401
    else:
        response_object = {
            'status': 'fail',
            'message': 'Provide a valid auth bearer token, professor'
        }
        return response_object, 401

