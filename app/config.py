import os


BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    CSRF_ENABLED = True
    CSRF_SESSION_KEY = '030e523b03e830abb8d99ede1c948bd6b73e84bfd6f08dca'
    SECRET_KEY = 'b145c5bd5a0d3c938c75a8b06e12d25540b72ed00da25d61'
    DEBUG = False
    THREADS_PER_PAGE = 2
    SERVER_NAME = 'localhost:8888'
    SWAGGER_UI_DOC_EXPANSION = 'list'
    VALIDATE = True
    MASK_SWAGGER = False
    ERROR_404_HELP = False


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'elearning-dev.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True

class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'elearning-test.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'elearning-prod.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config_by_name = dict(
        dev=DevelopmentConfig,
        test=TestingConfig,
        prod=ProductionConfig
    )