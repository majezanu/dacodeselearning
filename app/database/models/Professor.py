from app import bcrypt
from app.database import db
from .Base import Base
import datetime
import jwt
from ..repository.BlackListTokenRepository import BlackListTokenRepository
from ...config import Config


class Professor(Base):
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=True)

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    @staticmethod
    def encode_auth_token(professor_id):
        """ Generates the Auth Token :return: string """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': professor_id
            }
            return jwt.encode(payload, Config.SECRET_KEY, algorithm='HS256')
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """ Decodes the auth token :param auth_token: :return: integer|string """
        try:
            payload = jwt.decode(auth_token, Config.SECRET_KEY)
            is_blacklisted_token = BlackListTokenRepository.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again'
