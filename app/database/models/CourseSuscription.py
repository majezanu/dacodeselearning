from app.database import db
from app.database.models.Base import Base


class CourseSuscription(Base):

    __tablename__ = 'course_suscription'

    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    student_id = db.Column(db.Integer, db.ForeignKey('student.id'))
    approved = db.Column(db.Boolean, nullable=False)
    can_access = db.Column(db.Boolean, nullable=False)

    def __init__(self, course_id, student_id, approved, can_access):
        self.course_id = course_id
        self.student_id = student_id
        self.approved = approved
        self.can_access = can_access
