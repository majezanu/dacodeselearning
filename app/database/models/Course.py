from app.database import db
from app.database.models.Base import Base



class Course(Base):
    __tablename__ = 'course'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)
    previous_course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=True)

    previous_course = db.relationship("Course", remote_side=[id])
    lessons = db.relationship("Lesson", back_populates="course")
    students = db.relationship("Student", secondary='course_suscription')