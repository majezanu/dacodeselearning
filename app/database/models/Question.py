from app.database import db
from sqlalchemy.orm import relationship
from .Base import Base


class Question(Base):
    __tablename__ = "question"
    name = db.Column(db.String(255), nullable=False)
    score = db.Column(db.Integer, nullable=True)
    lesson_id = db.Column(db.Integer, db.ForeignKey('lesson.id'))
    lesson = relationship("Lesson", back_populates="questions")