from app.database import db
from .Base import Base


class LessonSuscription(Base):
    __tablename__ = 'lesson_suscription'

    lesson_id = db.Column(db.Integer, db.ForeignKey('lesson.id'))
    student_id = db.Column(db.Integer, db.ForeignKey('student.id'))
    passed = db.Column(db.Boolean, nullable=False)
    can_access = db.Column(db.Boolean, nullable=False)

    def __init__(self, lesson_id, student_id, passed, can_access):
        self.lesson_id = lesson_id
        self.student_id = student_id
        self.passed = passed
        self.can_access = can_access