from app.database import db
from .Base import Base


class Student(Base):
    __tablename__ = 'student'

    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    courses = db.relationship("Course", secondary='course_suscription')
    lessons = db.relationship("Lesson", secondary='lesson_suscription')