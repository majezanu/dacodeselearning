from app.database import db
from sqlalchemy.orm import relationship
from .Base import Base


class Lesson(Base):
    __tablename__ = 'lesson'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    previous_lesson_id = db.Column(db.Integer, db.ForeignKey('lesson.id'), nullable=True)
    approval_score = db.Column(db.Integer, nullable=True)

    previous_lesson = relationship("Lesson", remote_side=[id])
    course = relationship("Course", back_populates="lessons")
    questions = relationship("Question", back_populates="lesson")
