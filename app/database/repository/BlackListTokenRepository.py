from app.database import db
from app.database.models.BlackListToken import BlackListToken


class BlackListTokenRepository:

    def __init(self):
        self

    @staticmethod
    def get_blacklist_by_token(auth_token):
        return BlackListToken.query.filter_by(token=str(auth_token)).first()

    @staticmethod
    def save_token(token):
        blacklist_token = BlackListToken(token=token)
        try:
            # insert the token
            db.session.add(blacklist_token)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'Successfully logged out.'
            }
            return response_object, 200
        except Exception as e:
            response_object = {
                'status': 'fail',
                'message': e
            }
            return response_object, 200

    @staticmethod
    def check_blacklist(auth_token):
        res = BlackListTokenRepository.get_blacklist_by_token(auth_token)
        if res:
            return True
        else:
            return False
