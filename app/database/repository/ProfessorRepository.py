from app.database import db
from app.database.models.Professor import Professor

class ProfessorRepository:

    @staticmethod
    def find_one_by_name(name):
        return Professor.query.filter_by(name=name).first()

    @staticmethod
    def find_one_by_email(email):
        return Professor.query.filter_by(email=email).first()

    @staticmethod
    def save(professor):
        db.session.add(professor)
        db.session.commit()
        return ProfessorRepository.generate_token(professor)

    @staticmethod
    def all():
        return Professor.query.all()

    @staticmethod
    def find_one(id):
        return Professor.query.filter_by(id=id).first()

    @staticmethod
    def generate_token(professor):
        try:
            # generate the auth token
            auth_token = professor.encode_auth_token(professor.id)
            response_object = {
                'status': 'success',
                'message': 'Successfully registered.',
                'Authorization': auth_token.decode()
            }
            return response_object, 201
        except Exception as e:
            response_object = {
                'status': 'fail',
                'message': 'Some error occurred. Please try again.'
            }
            return response_object, 401