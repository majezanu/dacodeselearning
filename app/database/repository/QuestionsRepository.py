from app.database import db
from app.database.models.Question import Question


class QuestionRepository:

    @staticmethod
    def all():
        return Question.query.all()

    @staticmethod
    def find_one_by_name(name):
        return Question.query.filter_by(name=name).first()

    @staticmethod
    def save(question):
        db.session.add(question)
        db.session.commit()
        return question

    @staticmethod
    def find_one(id):
        return Question.query.filter_by(id=id).first()

    @staticmethod
    def update(question):
        db.session.commit()

    @staticmethod
    def delete(question):
        db.session.delete(question)
        db.session.commit()

