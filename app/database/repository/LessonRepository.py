from app.database import db
from app.database.models.Lesson import Lesson


class LessonRepository:

    @staticmethod
    def all():
        return Lesson.query.all()

    @staticmethod
    def find_one_by_name(name):
        return Lesson.query.filter_by(name=name).first()

    @staticmethod
    def save(lesson):
        db.session.add(lesson)
        db.session.commit()
        return lesson

    @staticmethod
    def find_one(id):
        return Lesson.query.filter_by(id=id).first()

    @staticmethod
    def update(lesson):
        db.session.commit()

    @staticmethod
    def delete(lesson):
        db.session.delete(lesson)
        db.session.commit()

