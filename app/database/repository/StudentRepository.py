from app.database import db
from app.database.models.Student import Student


class StudentRepository:

    @staticmethod
    def all():
        return Student.query.all()

    @staticmethod
    def find_one_by_name(name):
        return Student.query.filter_by(name=name).first()

    @staticmethod
    def save(student):
        db.session.add(student)
        db.session.commit()
        return student

    @staticmethod
    def find_one(id):
        return Student.query.filter_by(id=id).first()

    @staticmethod
    def update(student):
        db.session.commit()

    @staticmethod
    def delete(student):
        db.session.delete(student)
        db.session.commit()

