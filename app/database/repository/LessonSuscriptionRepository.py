from app.database import db
from app.database.models.Student import Student
from app.database.models.Course import Course
from app.database.models.LessonSuscription import LessonSuscription


class LessonSuscriptionRepository:

    @staticmethod
    def is_previous_passed(student_id, previous_lesson_id):
        previous_course = LessonSuscription.find_one_by_student_and_course(student_id, previous_lesson_id)
        return previous_course.approved

    @staticmethod
    def find_one_by_student_and_lesson(student_id, lesson_id):
        return LessonSuscription.query.filter(LessonSuscription.student_id == student_id).filter(LessonSuscription.lesson_id == lesson_id).first()

    @staticmethod
    def save(suscription):
        db.session.add(suscription)
        db.session.commit()