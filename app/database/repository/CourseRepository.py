from app.database import db
from app.database.models.Course import Course


class CourseRepository:

    @staticmethod
    def all():
        return Course.query.all()

    @staticmethod
    def find_one_by_name(name):
        return Course.query.filter_by(name=name).first()

    @staticmethod
    def save(course):
        db.session.add(course)
        db.session.commit()
        return course

    @staticmethod
    def find_one(id):
        return Course.query.filter_by(id=id).first()

    @staticmethod
    def update(course):
        db.session.commit()

    @staticmethod
    def delete(course):
        db.session.delete(course)
        db.session.commit()

