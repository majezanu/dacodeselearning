from app.database import db
from app.database.models.Student import Student
from app.database.models.Course import Course
from app.database.models.CourseSuscription import CourseSuscription

class CourseSuscriptionRepository:

    @staticmethod
    def is_previous_passed(student_id, previous_course_id):
        previous_course = CourseSuscriptionRepository.find_one_by_student_and_course(student_id, previous_course_id)
        return previous_course.approved

    @staticmethod
    def find_one_by_student_and_course(student_id, course_id):
        return CourseSuscription.query.filter(CourseSuscription.student_id == student_id).filter(CourseSuscription.course_id == course_id).first()

    @staticmethod
    def save(suscription):
        db.session.add(suscription)
        db.session.commit()