from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from .models import Professor, Course, Lesson, Question, Student, CourseSuscription, LessonSuscription


def create(app):
    db.init_app(app)
    db.create_all()


def reset():
    db.drop_all()
    db.create_all()
