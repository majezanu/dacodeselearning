import unittest
import datetime

from app import db
from app.database.models.Professor import Professor
from app.database.repository.ProfessorRepository import ProfessorRepository
from app.test.base import BaseTestCase


class TestProfessorModel(BaseTestCase):

    def test_encode_auth_token(self):

        professor = Professor(
            name='test',
            email='test@test.com',
            password='12345'
        )
        ProfessorRepository.save(professor)
        auth_token = professor.encode_auth_token(professor.id)
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        professor = Professor(
            name='test',
            email='test@test.com',
            password='12345'
        )
        ProfessorRepository.save(professor)
        auth_token = professor.encode_auth_token(professor.id)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertTrue(Professor.decode_auth_token(auth_token.decode("utf-8")) == 1)


if __name__ == '__main__':
    unittest.main()