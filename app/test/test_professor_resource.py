import unittest
import json
from app.test.base import BaseTestCase


def get_all_professors(self):
    return self.client.get(
        "api/professors/",
        content_type='application/json'
    )


def register_professor(self):
    return self.client.post(
        "api/professors/",
        data=json.dumps(
            dict(
                email='test2@test.com',
                name='test2',
                password='12345'
            )
        ),
        content_type='application/json'
    )


def login_user(self):
    return self.client.post(
        'api/professors/login',
        data=json.dumps(
            dict(
                email='test2@test.com',
                password='12345'
            )
        ),
        content_type='application/json'
    )


class TestProfessorBlueprint(BaseTestCase):
    def test_get_all_professors(self):
        with self.client:
            professors = get_all_professors(self)
            response_data = json.loads(professors.data.decode())
            self.assertTrue(isinstance(response_data, list))

    def test_registered_professor_login(self):
        """ Test for login of registered-professor """
        with self.client:
            professor = register_professor(self)
            response_data = json.loads(professor.data.decode())
            self.assertTrue(response_data[u'Authorization'])
            self.assertEqual(professor.status_code, 201)

            login_response = login_user(self)
            data = json.loads(login_response.data.decode())
            self.assertTrue(data['Authorization'])
            self.assertEqual(login_response.status_code, 200)


if __name__ == '__main__':
    unittest.main()