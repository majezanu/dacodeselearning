from flask_testing import TestCase
from app import main, db
from manage import app
from ..config import config_by_name


class BaseTestCase(TestCase):
    """ Base Tests """

    def create_app(self):
        self.app = app.config.from_object(config_by_name['test'])
        return app

    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

