import os
import unittest

from flask import current_app
from flask_testing import TestCase

from manage import app
from app.config import BASE_DIR
from app.config import config_by_name, Config, DevelopmentConfig, TestingConfig


class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app.config.from_object(config_by_name['dev'])
        return app

    def test_app_is_development(self):
        self.assertTrue(app.config['SECRET_KEY'] is Config.SECRET_KEY)
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertFalse(current_app is None)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == DevelopmentConfig.SQLALCHEMY_DATABASE_URI)


class TestTestingConfig(TestCase):
    def create_app(self):
        app.config.from_object(config_by_name['test'])
        return app

    def test_app_is_testing(self):
        self.assertTrue(app.config['SECRET_KEY'] is Config.SECRET_KEY)
        self.assertTrue(app.config['DEBUG'])
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == TestingConfig.SQLALCHEMY_DATABASE_URI)


class TestProductionConfig(TestCase):
    def create_app(self):
        app.config.from_object(config_by_name['prod'])
        return app

    def test_app_is_production(self):
        self.assertTrue(app.config['DEBUG'] is False)


if __name__ == '__main__':
    unittest.main()