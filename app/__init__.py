import logging.config
import os
from flask_bcrypt import Bcrypt
bcrypt = Bcrypt()
from flask import Flask, Blueprint
from .config import config_by_name
from app.api.professors.endpoints import ns as professors_namespace
from app.api.courses.endpoints import ns as courses_namespace
from app.api.lessons.endpoints import ns as lessons_namespace
from app.api.students.endpoints import ns as students_namespace
from app.api.questions.endpoints import ns as questions_namespace
from app.api.restplus import api
from app.database import db, create


logging_config_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../logging.conf'))
logging.config.fileConfig(logging_config_path)
log = logging.getLogger(__name__)


def configure(flask_app, config_name):
    flask_app.config.from_object(config_by_name[config_name])


def initialize(flask_app, config_name):
    configure(flask_app, config_name)

    blueprint = Blueprint('api_blueprint', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(professors_namespace)
    api.add_namespace(courses_namespace)
    api.add_namespace(lessons_namespace)
    api.add_namespace(students_namespace)
    api.add_namespace(questions_namespace)
    flask_app.register_blueprint(blueprint)
    with flask_app.app_context():
        create(flask_app)
        bcrypt.init_app(flask_app)


def main(config_name):
    app = Flask(__name__)
    initialize(app, config_name)
    log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    return app
